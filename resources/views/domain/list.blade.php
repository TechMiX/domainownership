@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Domains</div>

                    <div class="card-body">
                        <a href="{{ route('domains.create') }}">
                            <button type="button" class="btn btn-primary">Add Domain</button>
                        </a>
                        <br/>
                        <br/>
                        <div class="list-group">
                            @if ($domains->count() < 1)
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">You dont have any domains! :(</h5>
                                </div>
                            @else
                                @foreach ($domains as $domain)
                                    <a href="{{ route('domains.show', $domain->id) }}" class="list-group-item list-group-item-action flex-column align-items-start ">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1">{{ $domain->name }}</h5>
                                            <small>{{ $domain->created_at->diffForHumans() }}</small>
                                        </div>
                                        @if ($domain->hasAtLeastOneVerification())
                                            <span class="badge badge-success">Verified</span>
                                        @else
                                            <span class="badge badge-warning">Not Verified</span>
                                        @endif
                                    </a>
                                @endforeach
                                {{ $domains->links() }}
                            @endif

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
