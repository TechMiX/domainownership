@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Domain Details') }}</div>

                    <div class="card-body">
                        @error('duplicate')
                        <div class="alert alert-warning" role="alert">
                            {{ $errors->first('duplicate') }}
                        </div>
                        @enderror
                        <div class="list-group">
                            <div class="list-group-item list-group-item-action flex-column align-items-start ">
                                <div class="d-flex w-100">
                                    <small>Name</small>
                                </div>
                                <h5 class="mb-1">{{ $domain->name }}</h5>
                            </div>

                            @if ($domain->verifications->count() > 0)
                                @foreach ($domain->verifications as $verification)
                                    <div class="list-group-item list-group-item-action flex-column align-items-start ">
                                        <div class="d-flex w-100">
                                            <small>Verification Type</small>
                                        </div>
                                        <h5 class="mb-1">{{ config('enums.domain_verification_types')[$verification->type] }}</h5>

                                        <div class="d-flex w-100">
                                            <small>Token</small>
                                        </div>
                                        <h5 class="mb-1">{{ $verification->token }}</h5>

                                        <div class="d-flex w-100">
                                            <small>Status</small>
                                        </div>

                                        @if ($verification->status == 1)
                                            <span class="badge badge-success">Verified</span>
                                        @else
                                            <span class="badge badge-warning">Not Verified</span>
                                        @endif

                                        <a href="{{ route('domains.checkverification', $verification->id) }}">
                                            <button type="button" class="btn btn-primary">Refresh</button>
                                        </a>
                                    </div>
                                @endforeach
                            @else
                                <div class="list-group-item list-group-item-action flex-column align-items-start ">
                                    <span class="badge badge-warning">Not Verified</span>
                                </div>
                            @endif
                        </div>
                        <br/>
                        <a href="{{ route('domains.addverification', $domain->id) }}">
                            <button type="button" class="btn btn-primary">Add Verification</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
