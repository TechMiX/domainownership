<?php

return [
    'domain_verification_types' => [
        0 => 'DNS TXT Record',
        1 => 'Special Webpage'
    ],
    'domain_verification_status' => [
        0 => 'Not Verified',
        1 => 'Verified',
    ]
];
