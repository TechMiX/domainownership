<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
        'user_id',
        'name'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function verifications()
    {
        return $this->hasMany('App\DomainVerification');
    }

    public function hasAtLeastOneVerification()
    {
        foreach ($this->verifications as $verification) {
            if ($verification->isVerified())
                return true;
        }
        return false;
    }
}
