<?php

namespace App\DomainVerification;

use App\Domain;

interface BasicInterface
{

    public function generate(Domain $domain);

    public function check();

}
