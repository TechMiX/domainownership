<?php

namespace App\DomainVerification;

use App\DomainVerification\Types as DomainVerificationTypes;

class Factory
{

    public static function getInstanceByType($type)
    {
        switch ($type) {
            case DomainVerificationTypes::DNS:
                return new DNS();
            case DomainVerificationTypes::WEB_PAGE:
            default:
                return null;
        }
    }
}
