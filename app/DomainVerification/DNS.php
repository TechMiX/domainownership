<?php

namespace App\DomainVerification;

use App\Domain;
use App\DomainVerification;
use Carbon\Carbon;
use Illuminate\Support\Str;

class DNS extends DomainVerification implements BasicInterface
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->type = Types::DNS;
    }

    public function generate(Domain $domain)
    {
        $this->domain_id = $domain->id;
        $this->token = Str::random(30);
        $this->save();
    }

    public function check() : bool
    {
        $dns_query_result = dns_get_record($this->domain->name, DNS_TXT);
        if (!empty($dns_query_result)) {
            foreach ($dns_query_result as $dns_record) {
                if (isset($dns_record["txt"]) && $dns_record["txt"] == $this->token) {
                    $this->status = Status::VERIFIED;
                    $this->last_verified = Carbon::now();
                    $this->save();
                    return true;
                }
            }
        }

        $this->status = Status::NOT_VERIFIED;
        $this->save();
        return false;
    }
}
