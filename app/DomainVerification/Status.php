<?php

namespace App\DomainVerification;


class Status
{
    public const NOT_VERIFIED = 0;
    public const VERIFIED = 1;
}
