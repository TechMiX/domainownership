<?php

namespace App\Http\Controllers;

use App\Domain;
use App\DomainVerification;
use App\DomainVerification\Factory as DomainVerificationFactory;
use App\DomainVerification\Types as DomainVerificationTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class DomainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domains = Auth::user()->domains()->simplePaginate(5);
        return view('domain.list')->with('domains', $domains);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('domain.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                'string',
                'max:255',
                'regex:/^[a-zA-Z0-9]+([a-zA-Z0-9\-]{0,2}[a-zA-Z0-9]+|[a-zA-Z0-9]*)*\.[a-zA-Z]{2,}$/i'
            ],
        ]);
        $validator->validate();

        $name = $request->get('name');
        $name = strtolower($name);

        $duplicate_domain = Auth::user()->domains()->where(['name' => $name])->get();
        if ( !$duplicate_domain->isEmpty() ) {
            return back()->withErrors(['duplicate' => 'Already added this domain!']);
        }

        Domain::create([
            'name' => $name,
            'user_id' => Auth::user()->id
        ]);

        return redirect()->route('domains.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function show(Domain $domain)
    {
        Gate::authorize('domain', $domain);

        return view('domain.show')->with('domain', $domain);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function edit(Domain $domain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Domain $domain)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function destroy(Domain $domain)
    {
        //
    }

    public function addVerification(Domain $domain)
    {
        Gate::authorize('domain', $domain);

        $duplicate_verification = $domain->verifications()->where(['type' => DomainVerificationTypes::DNS])->get();
        if ( !$duplicate_verification->isEmpty() ) {
            return back()->withErrors(['duplicate' => 'Already added verification!']);
        }

        $new_domain_verification = DomainVerificationFactory::getInstanceByType(DomainVerificationTypes::DNS);
        $new_domain_verification->generate($domain);

        return redirect()->route('domains.show', [$domain]);
    }

    public function checkVerification(DomainVerification $verification)
    {
        Gate::authorize('domain', $verification->domain);

        $verification->check();

        return redirect()->route('domains.show', [$verification->domain]);
    }
}
