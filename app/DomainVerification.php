<?php

namespace App;

use App\DomainVerification\Factory as DomainVerificationFactory;
use App\DomainVerification\Status;
use Illuminate\Database\Eloquent\Model;

class DomainVerification extends Model
{
    protected $table = 'domain_verifications';

    protected $fillable = [
        'domain_id',
        'type',
        'token',
        'status',
        'last_verified'
    ];

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function isVerified() : bool
    {
        return ($this->status == Status::VERIFIED);
    }

    public function newFromBuilder($attributes = [], $connection = null)
    {
        $model = DomainVerificationFactory::getInstanceByType($attributes->type);

        $model->exists = true;
        $model->setRawAttributes((array) $attributes, true);
        $model->setConnection($connection ?: $this->getConnectionName());
        $model->fireModelEvent('retrieved', false);

        return $model;
    }
}
